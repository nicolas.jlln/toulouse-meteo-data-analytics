# Optimisations :

# Questions / Réponses :

1. Combien y a-t-il d’enregistrements en 2021 ?

Il y a __1196680__ enregistrements en 2021.

<details>
<summary>Requête associée</summary>

``` sql
SELECT
  COUNT(*) as records
FROM
  `toulouse-meteo-data-analytics.ds_toulouse_meteo_data.tb_telemetry`
WHERE
  heure_de_paris BETWEEN "2021-01-01" and "2021-12-31"
```
</details>

---

2. Pourcentage de stations qui ont été mise à jour hier

Je suis le 07/12/2023 lors de l'exécution de la requête. La veille est le 06/12/2023.

Ce jour, 5 stations ont été mises à jour sur les 49 enregistrées sur `tb_stations`.
Cela fait donc un pourcentage de __10,20%__

<details>
<summary>Requête associée</summary>

``` sql
WITH
  updated_stations AS (
  SELECT
    DISTINCT(station_id) AS station_id
  FROM
    `toulouse-meteo-data-analytics.ds_toulouse_meteo_data.tb_telemetry`
  WHERE
    Date(heure_de_paris) = DATE_SUB(CURRENT_DATE(), INTERVAL 1 DAY)),
  stations_count AS (
  SELECT
    DISTINCT(slug) AS station_id
  FROM
    `toulouse-meteo-data-analytics.ds_toulouse_meteo_data.tb_stations`)
SELECT
  COUNT(updated_stations.station_id) AS updated_stations_count,
  COUNT(stations_count.station_id) AS all_stations_count,
  COUNT(updated_stations.station_id) / COUNT(stations_count.station_id) * 100 AS percentage
FROM
  stations_count
LEFT JOIN
  updated_stations
ON
  stations_count.station_id = updated_stations.station_id
```
</details>

---

3. Liste des stations (titre, description) dont la pression max en 2021 a été inférieure à 100000.

__1__ seule station est remontée lors de l'exécution de la requête.

Titre : "50 Station météo Blagnac Quinze Sols"
Description : `<p>Ce jeu de données est issu du capteur n°50 situé sur le site de la base de loisir Quinze Sols (Commune de Blagnac) qui fait parti d'un  réseau de stations météo situées sur la métropole Toulousaine en vue  d'étudier le phénomène d'Ilot de Chaleur Urbain (ICU)</p>`

<details>
<summary>Requête associée</summary>

``` sql
WITH
  telemetry AS (
  SELECT
    station_id,
    MAX(pression) AS max_pression
  FROM
    `toulouse-meteo-data-analytics.ds_toulouse_meteo_data.tb_telemetry`
  WHERE
    heure_de_paris BETWEEN "2021-01-01"
    AND "2021-12-31"
  GROUP BY
    station_id)
SELECT
  stations.title,
  stations.description
FROM
  telemetry
LEFT JOIN
  `toulouse-meteo-data-analytics.ds_toulouse_meteo_data.tb_stations` AS stations
ON
  telemetry.station_id = stations.slug
WHERE
  telemetry.max_pression < 100000
```
</details>

---

4. Liste des station_id de la table télémétries (tb_telemetry) qui ne sont pas référencés dans la table référentiel des stations (tb_stations)

Il semblerait qu'il y ait __5__ stations de la table `tb_telemetry` qui soient non référencées dans la table référentiel :

* 65-station-meteo-toulouse-life-gastou
* 06-station-meteo-toulouse-ecluse
* 63-station-meteo-toulouse-life-marechal-juin
* 20-station-meteo-mondonville-ecole
* 38-station-meteo-toulouse-parc-jardin-des-plantes
<details>
<summary>Requête associée</summary>

``` sql
WITH
  distinct_telemetry AS (
  SELECT
    DISTINCT(station_id) as station_id
  FROM
    `toulouse-meteo-data-analytics.ds_toulouse_meteo_data.tb_telemetry` )
SELECT
  stations.slug
FROM
  distinct_telemetry
RIGHT JOIN
  `toulouse-meteo-data-analytics.ds_toulouse_meteo_data.tb_stations` AS stations
ON
  distinct_telemetry.station_id = stations.slug
where
  distinct_telemetry.station_id is NULL
```
</details>

---

5. Moyenne de la température par station la semaine dernière calendaire

6. A quelle heure la pression atmosphérique est-elle au minimum en général ?

7. Temps moyen entre 2 mesures par station

8. Donner la moyenne de la température, le premier mois d’exportation pour chaque station

9. Donner la somme cumulée de la pluie sur une fenêtre glissante de 3 jours (précédents), pour chaque jours et chaque station de la semaine n°5 de l’année 2021

10. Donner la liste des plages de jours consécutifs de canicules (température supérieur à 25 degré toute la journée) pour chacune des stations

